
read -p "Voulez vous définir les variable proxy de l'université ?[y/n]" reponse
if echo "$reponse" | grep -iq "^y" ;then
    export http_proxy="http://wwwcache.univ-orleans.fr:3128/"
    export https_proxy="http://wwwcache.univ-orleans.fr:3128/"
fi
read -p "voulez vous (re)créer le virtualenv ?[y/n]" reponse
if echo "$reponse" | grep -iq "^y" ;then
    rm -rf venv
    virtualenv -p python3 venv
fi

source venv/bin/activate
pip install -r requirements.txt
export FLASK_APP=./app.py
export FLASK_DEBUG=1


echo "Vous pouvez lancer votre application en faisant : flask run"
